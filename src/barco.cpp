#include "barco.hpp"

Barco::Barco(){
	latitude = 0;
	longitude = 0;
	orientacao = " ";
	destruido = false;

}
Barco::~Barco(){
}

int Barco::get_latitude(){
return latitude;}

int Barco::get_longitude(){
return longitude;}

string Barco::get_orientacao(){
return orientacao;}

bool Barco::get_destruido(){
return destruido;}



void Barco::set_latitude(int latitude){
	this->latitude=latitude;
}
void Barco::set_longitude(int longitude){
	this->longitude=longitude;
}
void Barco::set_orientacao(string orientacao){
	this->orientacao=orientacao;
}
void Barco::set_destruido(bool destruido){
	this->destruido=destruido;
}
